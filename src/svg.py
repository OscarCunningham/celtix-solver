# Art stolen from Andrew Taylor's Celtix, https://www.andrewt.net/puzzles/celtix/

import drawsvg as draw
from PIL import Image
import numpy as np

from io import BytesIO

Image.MAX_IMAGE_PIXELS = None

shapes = {
    ((-1, -1), (0, 1)): ('fortyFive','translate(264.58333,264.58333) rotate(90) scale(-1,1)'),
    ((-1, -1), (1, 0)): ('fortyFive','translate(264.58333,264.58333) rotate(180)'),
    ((-1, -1), (1, 1)): ('diagCentre','translate(264.58333,0) rotate(90)'),
    ((-1, 0), (0, -1)): ('straightCorner','translate(264.58333,0) rotate(90)'),
    ((-1, 0), (0, 1)): ('straightCorner','rotate(0)'),
    ((-1, 0), (1, -1)): ('fortyFive','translate(0,264.58333) scale(1,-1)'),
    ((-1, 0), (1, 0)):('straightCentre','translate(264.58333,0) rotate(90)'),
    ((-1, 0), (1, 1)): ('fortyFive','rotate(0)'),
    ((-1, 1), (0, -1)): ('fortyFive','translate(264.58333,0) rotate(90)'),
    ((-1, 1), (1, -1)): ('diagCentre','rotate(0)'),
    ((-1, 1), (1, 0)): ('fortyFive','translate(264.58333,0) scale(-1,1)'),
    ((0, -1), (0, 1)):('straightCentre','rotate(0)'),
    ((0, -1), (1, 0)): ('straightCorner','translate(264.58333,264.58333) rotate(180)'),
    ((0, -1), (1, 1)): ('fortyFive','rotate(270) scale(-1,1)'),
    ((0, 1), (1, -1)): ('fortyFive','translate(0,264.58333) rotate(270)'),
    ((0, 1), (1, 0)): ('straightCorner','translate(0,264.58333) rotate(270)'),
}

colours = {
    1:'red',
    2:'#ed0',
    3:'#0c0',
    4:'#00f',
    5:'#c0f',
}

def make_image(grid, walls, pip_locations):
    width, height = len(grid[0]), len(grid)

    x_spacing, y_spacing = 264.58333, 264.58333

    drawing_width, drawing_height = x_spacing*(width+1), y_spacing*(height+1)

    wall_length = 234.6

    d = draw.Drawing(drawing_width, drawing_height)
    d.append(draw.Rectangle(0, 0, drawing_width, drawing_height, fill='white'))

    for y in range(height):
        for x in range(width):
            if x in (0, width-1):
                if y % 2 == 0:
                    neighbour_0 = (0, 1)
                else:
                    neighbour_0 = (0, -1)
            else:
                if x%2 == 0:
                    neighbouring_wall_x = 2*x - 1
                else:
                    neighbouring_wall_x = 2*x + 1
                neighbouring_wall_y = y + (y^1)
                wall_type = walls[(neighbouring_wall_x, neighbouring_wall_y)]
                if wall_type == ' ':
                    neighbour_0 = (neighbouring_wall_x - 2*x, neighbouring_wall_y - 2*y)
                elif wall_type == '|':
                    neighbour_0 = (0, neighbouring_wall_y - 2*y)
                elif wall_type == '-':
                    neighbour_0 = (neighbouring_wall_x - 2*x, 0)
                else:
                    raise Exception('Unrecognised wall type')
                
            if y in (0, height-1):
                if x % 2 == 0:
                    neighbour_1 = (1, 0)
                else:
                    neighbour_1 = (-1, 0)
            else:
                if y%2 == 0:
                    neighbouring_wall_y = 2*y - 1
                else:
                    neighbouring_wall_y = 2*y + 1
                neighbouring_wall_x = x + (x^1)
                wall_type = walls[(neighbouring_wall_x, neighbouring_wall_y)]
                if wall_type == ' ':
                    neighbour_1 = (neighbouring_wall_x - 2*x, neighbouring_wall_y - 2*y)
                elif wall_type == '|':
                    neighbour_1 = (0, neighbouring_wall_y - 2*y)
                elif wall_type == '-':
                    neighbour_1 = (neighbouring_wall_x - 2*x, 0)
                else:
                    raise Exception('Unrecognised wall type')
            
            if neighbour_0 > neighbour_1:
                neighbour_0, neighbour_1 = neighbour_1, neighbour_0

            shape, transformation = shapes[(neighbour_0, neighbour_1)]
            colour = colours[grid[y][x]]

            if shape == 'fortyFive':    
                s = draw.Path(stroke_width=15, stroke='#000', fill=colour) \
                    .M(-26.458333, 26.458333) \
                    .h(52.916666) \
                    .h(26.458333) \
                    .c(136.359684, 0, 158.945264, 35.700849, 202.228884, 79.309907) \
                    .c(16.5352, 16.65953, 37.5233, 37.19777, 37.5233, 37.19777) \
                    .l(3.50664, 161.91786) \
                    .l(-140.84259,-0.49901) \
                    .c(0, 0, -29.57595, -27.59459, -37.36901, -36.35891) \
                    .c(-5.85679, -6.58673, 1.29392, 0.32989, -4.71051, -5.39526) \
                    .C(99.259419, 249.28762, 86.111621, 238.45044, 52.916666, 238.125) \
                    .c(-33.194957, -0.32544, -79.374999, 0, -79.374999, 0) \
                    .Z()
            elif shape=='straightCentre':
                s = draw.Rectangle(
                    -50,
                    -238.125,
                    350,
                    211.66666,
                    fill=colour,
                    stroke='#000',
                    stroke_width=15,
                    transform="rotate(90)"
                )
            elif shape=='straightCorner':
                s = draw.Path(stroke_width=15, stroke='#000', fill=colour) \
                    .M(-26.458333, 26.458333) \
                    .H(238.125) \
                    .V(291.04166) \
                    .H(26.458333) \
                    .V(238.125) \
                    .h(-52.916666) \
                    .Z()
            elif shape=='diagCentre':
                s = draw.Rectangle(
                    -292.92206,
                    -205.62973,
                    211.66666,
                    411.25946,
                    fill=colour,
                    stroke='#000',
                    stroke_width=15,
                    transform="rotate(-135)"
                )
            else:
                raise Exception('Shape not recognized')
            
            x_offset, y_offset = (0.5+x)*x_spacing, (0.5+y)*y_spacing
            s = draw.Use(s,0,0,transform=transformation)
            c = draw.ClipPath()
            c.append(draw.Rectangle(x_offset, y_offset, x_spacing, y_spacing))
            d.append(draw.Use(s, x_offset, y_offset, clip_path=c))

    for (x,y), wall_type in walls.items():
        if wall_type == ' ':
            if x % 4 == 1:
                x_offset, y_offset = (1+x/2)*x_spacing, (y/2)*y_spacing
                colour = colours[grid[y//2][x//2]]
                s = draw.Rectangle(
                    81.255341,
                    -392.71851,
                    211.66666,
                    411.25946,
                    fill=colour,
                    stroke='#000',
                    stroke_width=15,
                    transform="rotate(135)"
                )
                c = draw.ClipPath()
                c.append(draw.Rectangle(x_offset, y_offset, x_spacing, y_spacing))
                d.append(draw.Use(s, x_offset, y_offset, clip_path=c))

                x_offset, y_offset = (x/2)*x_spacing, (y/2+1)*y_spacing
                s = draw.Rectangle(
                    81.255341,
                    -392.71851,
                    211.66666,
                    411.25946,
                    fill=colour,
                    stroke='#000',
                    stroke_width=15,
                    transform="rotate(135)"
                )
                s = draw.Use(s,0,0,transform='translate(264.58333,264.58333) rotate(180)')
                c = draw.ClipPath()
                c.append(draw.Rectangle(x_offset, y_offset, x_spacing, y_spacing))
                d.append(draw.Use(s, x_offset, y_offset, clip_path=c))
            else:
                x_offset, y_offset = (x/2+1)*x_spacing, (y/2+1)*y_spacing
                colour = colours[grid[y//2][x//2+1]]
                s = draw.Rectangle(
                    81.255341,
                    -392.71851,
                    211.66666,
                    411.25946,
                    fill=colour,
                    stroke='#000',
                    stroke_width=15,
                    transform="rotate(135)"
                )
                s = draw.Use(s,0,0,transform='translate(264.58333,0) rotate(90)')
                c = draw.ClipPath()
                c.append(draw.Rectangle(x_offset, y_offset, x_spacing, y_spacing))
                d.append(draw.Use(s, x_offset, y_offset, clip_path=c))

                x_offset, y_offset = (x/2)*x_spacing, (y/2)*y_spacing
                s = draw.Rectangle(
                    81.255341,
                    -392.71851,
                    211.66666,
                    411.25946,
                    fill=colour,
                    stroke='#000',
                    stroke_width=15,
                    transform="rotate(135)"
                )
                s = draw.Use(s,0,0,transform='translate(0,264.58333) rotate(270)')
                c = draw.ClipPath()
                c.append(draw.Rectangle(x_offset, y_offset, x_spacing, y_spacing))
                d.append(draw.Use(s, x_offset, y_offset, clip_path=c))
        elif wall_type == '-':
            mid_x = (1+x/2)*x_spacing
            mid_y = (1+y/2)*y_spacing
            d.append(draw.Line(mid_x-wall_length/2, mid_y, mid_x+wall_length/2, mid_y, stroke='#888888', stroke_width=25.1, stroke_linecap='round'))
        elif wall_type == '|':
            mid_x = (1+x/2)*x_spacing
            mid_y = (1+y/2)*y_spacing
            d.append(draw.Line(mid_x, mid_y-wall_length/2, mid_x, mid_y+wall_length/2, stroke='#888888', stroke_width=25.1, stroke_linecap='round'))
        else:
            raise Exception('Wall type not recognised')
    for pip_colour, pips in pip_locations.items():
        for x,y in pips:
            colour = colours[pip_colour]
            x_offset, y_offset = (x+1)*x_spacing, (y+1)*y_spacing

            gradient = draw.RadialGradient(x_offset, y_offset, 90)
            gradient.add_stop(0.4, '#000', 1)
            gradient.add_stop(1, '#000', 0)

            d.append(draw.Circle(x_offset, y_offset, 90, fill=gradient))
            d.append(draw.Circle(x_offset, y_offset, 65, fill='#FFF'))
            d.append(draw.Circle(x_offset, y_offset, 55, fill=colour))

    # Render image large and then shrink it down, to avoid gaps between tiles
    d.set_pixel_scale(5)
    target_width, target_height = 36*(width+1), 36*(height+1)
    png_data = d.rasterize().png_data
    image = Image.open(BytesIO(png_data))
    image = SRGBResize(image,(target_width, target_height))

    return image

def SRGBResize(im, size):
        # This is stupid and probably doesn't matter, but PIL wrongly does image resizing in sRGB colour space.
        # So we convert to linear space, resize, and convert back again.
        # If they ever fix the resizing then ours will be broken.

        # Convert to numpy array of float
        arr = np.array(im, dtype=np.float32) / 255.0
        # Convert sRGB -> linear
        arr = np.where(arr <= 0.04045, arr/12.92, ((arr+0.055)/1.055)**2.4)
        # Resize using PIL
        arrOut = np.zeros((size[1], size[0], arr.shape[2]))
        for i in range(arr.shape[2]):
            chan = Image.fromarray(arr[:,:,i])
            chan = chan.resize(size, Image.Resampling.LANCZOS)
            arrOut[:,:,i] = np.array(chan).clip(0.0, 1.0)
        # Convert linear -> sRGB
        arrOut = np.where(arrOut <= 0.0031308, 12.92*arrOut, 1.055*arrOut**(1.0/2.4) - 0.055)
        # Convert to 8-bit
        arrOut = np.uint8(np.rint(arrOut * 255.0))
        # Convert back to PIL
        return Image.fromarray(arrOut)