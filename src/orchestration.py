from src.sat import setup_variables, add_wall_upper_bound, add_wall_lower_bound, setup_variables_disconnected
from src.grid_logic import build_solution, count_walls, wall_coordinates, merge_loops, optimise_solution
from src.solver import use_solver

def solve(clauses, number_of_variables, width, height, colour_variables, wall_variables, colours, pip_locations, timeout=None):
    assignment = use_solver(clauses, number_of_variables, timeout=timeout)

    if assignment is None:
        return None
    else:
        return build_solution(width, height, assignment, colour_variables, wall_variables, colours, pip_locations)

def find_minimal_solution(width, height, pip_locations):

    colours = set(pip_locations)

    lower_bound = 0
    best_disconnected_solution = None
    best_solution = None
    possible_walls = len(wall_coordinates(width, height))

    while True:
        best_disconnected_number_of_walls = count_walls(best_disconnected_solution['walls']) if best_disconnected_solution else None
        best_number_of_walls = count_walls(best_solution['walls']) if best_solution else None

        print(lower_bound, best_disconnected_number_of_walls, best_number_of_walls, possible_walls)
        if lower_bound > possible_walls:
            return None
        if best_solution and lower_bound == best_number_of_walls:
            return best_solution
        if best_solution and lower_bound > best_number_of_walls:
            raise Exception('Lower bound inconsistent with solution')
        
        if best_disconnected_solution is None or lower_bound < best_disconnected_number_of_walls:
            print('Finding disconnected solution')
            number_of_variables, clauses, colour_variables, wall_variables = setup_variables_disconnected(width, height, pip_locations)

            if best_disconnected_solution:
                upper_bound = (lower_bound + best_disconnected_number_of_walls)//2
            else:
                upper_bound = possible_walls

            number_of_variables, clauses = add_wall_upper_bound(wall_variables, upper_bound, number_of_variables, clauses, width, height)
            number_of_variables, clauses = add_wall_lower_bound(wall_variables, lower_bound, number_of_variables, clauses, width, height)

            disconnected_solution = solve(clauses, number_of_variables, width, height, colour_variables, wall_variables, colours, pip_locations)

            if disconnected_solution:
                number_of_walls = count_walls(disconnected_solution['walls'])
                if best_disconnected_number_of_walls is None or number_of_walls < best_disconnected_number_of_walls:
                    best_disconnected_solution = disconnected_solution
                    best_disconnected_number_of_walls = number_of_walls
                solution = optimise_solution(merge_loops(disconnected_solution))
                if solution:
                    number_of_walls = count_walls(solution['walls'])
                    if best_number_of_walls is None or number_of_walls < best_number_of_walls:
                        best_solution = solution
                        best_number_of_walls = number_of_walls
                    if best_disconnected_number_of_walls is None or number_of_walls < best_disconnected_number_of_walls:
                        best_disconnected_solution = solution
                        best_disconnected_number_of_walls = number_of_walls
            else:
                lower_bound = upper_bound + 1
        else:
            print('Finding connected solution')
            number_of_variables, clauses, colour_variables, wall_variables = setup_variables(width, height, pip_locations)

            if best_solution:
                upper_bound = (lower_bound + best_number_of_walls)//2
            else:
                upper_bound = possible_walls
            number_of_variables, clauses = add_wall_upper_bound(wall_variables, upper_bound, number_of_variables, clauses, width, height)
            number_of_variables, clauses = add_wall_lower_bound(wall_variables, lower_bound, number_of_variables, clauses, width, height)


            solution = optimise_solution(solve(clauses, number_of_variables, width, height, colour_variables, wall_variables, colours, pip_locations))

            if solution:
                number_of_walls = count_walls(solution['walls'])
                if best_number_of_walls is None or number_of_walls < best_number_of_walls:
                    best_solution = solution
                    best_number_of_walls = number_of_walls
                if best_disconnected_number_of_walls is None or number_of_walls < best_disconnected_number_of_walls:
                    best_disconnected_solution = solution
                    best_disconnected_number_of_walls = number_of_walls
            else:
                lower_bound = upper_bound + 1
