from pysat.card import CardEnc, EncType
from src.grid_logic import wall_coordinates, choose_pips, find_max_conection_lengths, wall_types

# pairwise    = 0  # Not valid encoding
# seqcounter  = 1
# sortnetwrk  = 2
# cardnetwrk  = 3
# bitwise     = 4  # Not valid encoding
# ladder      = 5  # Not valid encoding
# totalizer   = 6
# mtotalizer  = 7
# kmtotalizer = 8
# native      = 9  # Not valid encoding


master_encoding = EncType.totalizer
exactly_one_colour_encoding = master_encoding
exactly_one_wall_encoding = master_encoding
neighbour_encoding = master_encoding
upper_bound_encoding = master_encoding
lower_bound_encoding = master_encoding

def setup_variables(width, height, pip_locations):

    colours = set(pip_locations)
    chosen_pips = choose_pips(pip_locations)
    max_conection_lengths = find_max_conection_lengths(width, height, pip_locations, chosen_pips)

    number_of_variables = 0
    clauses = []

    wall_variables = dict()
    for (x,y) in wall_coordinates(width, height):
        for wall_type in wall_types:
            number_of_variables += 1
            wall_variables[(x,y,wall_type)] = number_of_variables
                    

        new_clauses = CardEnc.equals(
            lits=[wall_variables[(x,y,wall_type)] for wall_type in wall_types],
            bound = 1,
            top_id = number_of_variables,
            encoding = exactly_one_wall_encoding
        )
        number_of_variables = max(number_of_variables,new_clauses.nv)
        clauses += new_clauses.clauses

    connection_variables = dict()
    colour_variables = dict()
    for colour in colours:
        for connection_length in range(max_conection_lengths[colour]+1):
            for y in range(height):
                for x in range(width):
                    number_of_variables += 1
                    connection_variables[(x,y,colour,connection_length)] = number_of_variables
                    if connection_length == 0:
                        if (x,y) == chosen_pips[colour]:
                            clauses.append([connection_variables[(x,y,colour,connection_length)]])
                        else:
                            clauses.append([-connection_variables[(x,y,colour,connection_length)]])
                    else:
                        neighbouring_walls = []
                        neigbour_variables = [connection_variables[(x,y,colour,connection_length-1)]]
                        if x in (0, width - 1):
                            y1 = y^1
                            neigbour_variables.append(connection_variables[(x,y1,colour,connection_length-1)])
                        else:
                            if x%2 == 0:
                                neighbouring_wall_x = 2*x - 1
                            else:
                                neighbouring_wall_x = 2*x + 1
                            neighbouring_wall_y = y + (y^1)
                            neighbouring_walls.append((neighbouring_wall_x, neighbouring_wall_y))

                        if y in (0, height - 1):
                            x1 = x^1
                            neigbour_variables.append(connection_variables[(x1,y,colour,connection_length-1)])
                        else:
                            if y%2 == 0:
                                neighbouring_wall_y = 2*y - 1
                            else:
                                neighbouring_wall_y = 2*y + 1
                            neighbouring_wall_x = x + (x^1)
                            neighbouring_walls.append((neighbouring_wall_x, neighbouring_wall_y))

                        for neighbouring_wall_x, neighbouring_wall_y in neighbouring_walls:
                            for wall_type in wall_types:
                                number_of_variables += 1
                                neigbour_variable = number_of_variables
                                wall_variable = wall_variables[(neighbouring_wall_x, neighbouring_wall_y, wall_type)]
                                if wall_type == ' ':
                                    (x1, y1) = (neighbouring_wall_x - x, neighbouring_wall_y - y)
                                elif wall_type == '|':
                                    (x1, y1) = (x, neighbouring_wall_y - y)
                                elif wall_type == '-':
                                    (x1, y1) = (neighbouring_wall_x - x, y)
                                else:
                                    raise Exception('Unrecognised wall type')
                                connection_variable = connection_variables[(x1,y1,colour,connection_length-1)]
                                
                                clauses += [  # The neigbour_variable is true if and only if both the connection_variable and wall_variable are true
                                    [-neigbour_variable, connection_variable],
                                    [-neigbour_variable, wall_variable],
                                    [-connection_variable, -wall_variable, neigbour_variable]
                                ]

                                neigbour_variables.append(neigbour_variable)
                        
                        conditional_clauses = CardEnc.atleast(
                            lits = neigbour_variables,
                            bound = 1,
                            top_id = number_of_variables,
                            encoding = neighbour_encoding
                        )

                        number_of_variables = max(number_of_variables,new_clauses.nv)
                        for clause in conditional_clauses.clauses:
                            clauses.append([-connection_variables[(x,y,colour,connection_length)], *clause])

                        for neigbour_variable in neigbour_variables:
                            clauses.append([-neigbour_variable, connection_variables[(x,y,colour,connection_length)]])
                        

                    if connection_length == max_conection_lengths[colour]:
                        colour_variables[(x,y,colour)] = connection_variables[(x,y,colour,connection_length)]


    for colour, pips in pip_locations.items():
        for x,y in pips:
            if (x,y) != chosen_pips[colour]:
                clauses.append([colour_variables[(x,y,colour)]])

    for y in range(height):
        for x in range(width):
            new_clauses = CardEnc.equals(
                lits=[colour_variables[(x,y,colour)] for colour in colours],
                bound = 1,
                top_id = number_of_variables,
                encoding = exactly_one_colour_encoding
            )
            number_of_variables = max(number_of_variables,new_clauses.nv)
            clauses += new_clauses.clauses
    
    return number_of_variables, clauses, colour_variables, wall_variables

def setup_variables_disconnected(width, height, pip_locations):

    colours = set(pip_locations)

    number_of_variables = 0
    clauses = []

    colour_variables = dict()
    for y in range(height):
        for x in range(width):
            for colour in colours:
                number_of_variables += 1
                colour_variables[(x,y,colour)] = number_of_variables
            new_clauses = CardEnc.equals(
                lits=[colour_variables[(x,y,colour)] for colour in colours],
                bound = 1,
                top_id = number_of_variables,
                encoding = exactly_one_colour_encoding
            )
            number_of_variables = max(number_of_variables,new_clauses.nv)
            clauses += new_clauses.clauses

    for colour, pips in pip_locations.items():
        for x,y in pips:
            clauses.append([colour_variables[(x,y,colour)]])

    wall_variables = dict()
    for (x,y) in wall_coordinates(width, height):
        for wall_type in wall_types:
            number_of_variables += 1
            wall_variables[(x,y,wall_type)] = number_of_variables
            x_cell = x//2
            y_cell = y//2
            if wall_type == ' ':
                equal_pairs = (((x_cell,y_cell),(x_cell+1,y_cell+1)), ((x_cell+1,y_cell),(x_cell,y_cell+1)))
            elif wall_type == '|':
                equal_pairs = (((x_cell,y_cell),(x_cell,y_cell+1)), ((x_cell+1,y_cell),(x_cell+1,y_cell+1)))
            elif wall_type == '-':
                equal_pairs = (((x_cell,y_cell),(x_cell+1,y_cell)), ((x_cell,y_cell+1),(x_cell+1,y_cell+1)))
            else:
                raise Exception('Unrecognised wall type')
            for colour in colours:
                for (x_0, y_0), (x_1, y_1) in equal_pairs:
                    clauses.append([
                        -wall_variables[(x,y,wall_type)],
                        colour_variables[(x_0,y_0,colour)],
                        -colour_variables[(x_1,y_1,colour)],
                    ])
                    clauses.append([
                        -wall_variables[(x,y,wall_type)],
                        -colour_variables[(x_0,y_0,colour)],
                        colour_variables[(x_1,y_1,colour)],
                    ])
                    

        new_clauses = CardEnc.equals(
            lits=[wall_variables[(x,y,wall_type)] for wall_type in wall_types],
            bound = 1,
            top_id = number_of_variables,
            encoding = exactly_one_wall_encoding
        )
        number_of_variables = max(number_of_variables,new_clauses.nv)
        clauses += new_clauses.clauses

    for colour in colours:
        for x in range(width):
            if x%2 == 0:    
                clauses.append([
                    colour_variables[(x,0,colour)],
                    -colour_variables[(x+1,0,colour)],
                ])    
                clauses.append([
                    -colour_variables[(x,0,colour)],
                    colour_variables[(x+1,0,colour)],
                ])
                clauses.append([
                    colour_variables[(x,height-1,colour)],
                    -colour_variables[(x+1,height-1,colour)],
                ])    
                clauses.append([
                    -colour_variables[(x,height-1,colour)],
                    colour_variables[(x+1,height-1,colour)],
                ])

        for y in range(height):
            if y%2 == 0:
                clauses.append([
                    colour_variables[(0,y,colour)],
                    -colour_variables[(0,y+1,colour)],
                ])    
                clauses.append([
                    -colour_variables[(0,y,colour)],
                    colour_variables[(0,y+1,colour)],
                ])    
                clauses.append([
                    colour_variables[(width-1,y,colour)],
                    -colour_variables[(width-1,y+1,colour)],
                ])    
                clauses.append([
                    -colour_variables[(width-1,y,colour)],
                    colour_variables[(width-1,y+1,colour)],
                ])
    return number_of_variables, clauses, colour_variables, wall_variables
    

def add_wall_upper_bound(wall_variables, bound, number_of_variables, clauses, width, height):
    new_clauses = CardEnc.atmost(
        lits=[wall_variables[(x,y,wall_type)] for x,y in wall_coordinates(width, height) for wall_type in ('-','|')],
        bound = bound,
        top_id = number_of_variables,
        encoding = upper_bound_encoding
    )

    total_variables = max(number_of_variables,new_clauses.nv)
    clauses_with_bound = clauses + new_clauses.clauses
    
    return total_variables, clauses_with_bound

def add_wall_lower_bound(wall_variables, bound, number_of_variables, clauses, width, height):
    new_clauses = CardEnc.atleast(
        lits=[wall_variables[(x,y,wall_type)] for x,y in wall_coordinates(width, height) for wall_type in ('-','|')],
        bound = bound,
        top_id = number_of_variables,
        encoding = lower_bound_encoding
    )

    total_variables = max(number_of_variables,new_clauses.nv)
    clauses_with_bound = clauses + new_clauses.clauses
    
    return total_variables, clauses_with_bound