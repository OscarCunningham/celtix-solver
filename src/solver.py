import subprocess
import pathlib

def use_solver(clauses, number_of_variables, timeout=None):
    
    cnf_string = "p cnf " + str(number_of_variables) + " " + str(len(clauses)) + "\n"
    for clause in clauses:
        cnf_string += " ".join(str(literal) for literal in clause) + " 0\n"

    solver_path = pathlib.Path(__file__).parent.parent / 'kissat' / 'build' / 'kissat'
    command = [solver_path, '--unsat']
    if timeout is not None:
        command.append(f'--time={timeout}')
    completed_process = subprocess.run(
        command,
        input=cnf_string,
        capture_output=True,
        encoding="utf-8"
    )
    if completed_process.returncode not in (10,20):
        print(completed_process.stderr)
        raise Exception(f'Unrecognised status code {completed_process.returncode}')
    out = completed_process.stdout

    solution = out.split("\ns ")[1].split("\nc")[0].split("\nv ")
    
    if solution[0] == "SATISFIABLE":
        return [int(l) for l in " ".join(solution[1:]).split(" ")[:-1]] # looks like [-1, 2, 3,-4, 5, ...]
    elif solution[0] == "UNSATISFIABLE":
        return None
    else:
        raise Exception('Unrecognised solver status')