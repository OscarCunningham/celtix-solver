from selenium import webdriver
from bs4 import BeautifulSoup

colour_mapping = {
    'p1': 1,  # Red
    'p2': 4,  # Blue
    'p3': 3,  # Green
    'p4': 5,  # Purple
    'p5': 2,  # Yellow
}

def scrape_celtix(url=None):
    if url is None:
        url = 'https://www.andrewt.net/puzzles/celtix/'
    driver = webdriver.Firefox()
    driver.get(url)
    soup = BeautifulSoup(driver.page_source, features='html.parser')
    driver.quit()

    puzzle_id = str(soup.find('html').find('body').find('main').find('header').find('h1').find(attrs={'id': 'puzzle-id'}).string)
    if puzzle_id[0] != '#':
        raise Exception(f'Unexpected puzzle_id {repr(puzzle_id)}')
    day = int(puzzle_id[1:])
    
    width = 0
    height = 0
    pip_locations = dict()

    for cell in soup.find('html').find('body').find('main').find(attrs={'id': 'container'}).find('div').children:
        x = int(cell['data-x'])
        y = int(cell['data-y'])

        width = max(width, x+1)
        height = max(height, y+1)
        for child in cell.children:
            if child['class'][0] == 'bead':
                colour = colour_mapping[child['class'][1]]
                if colour not in pip_locations:
                    pip_locations[colour] = []
                pip_locations[colour].append((x, y))

    # Deduplicate pips in same location
    for colour in pip_locations:
        pip_locations[colour] = list(set(pip_locations[colour]))

    return day, width, height, pip_locations