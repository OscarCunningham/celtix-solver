import itertools
from copy import deepcopy
import random

wall_types = (' ', '|', '-')

def wall_coordinates(width, height):
    return [(x,y) for y in range(2*height - 2) for x in range(2*width - 2) if x % 2 and y % 2 and (x+y)%4 == 0]

def choose_pips(pip_locations):
    return {colour:pips[0] for colour, pips in pip_locations.items()}

def find_max_conection_lengths(width, height, pip_locations, chosen_pips):
    colours = set(pip_locations)

    max_conection_lengths = dict()
    best_loop_sizes = dict()
    for colour in colours:
        distances = {(x0, y0):dict() for x0, y0 in pip_locations[colour]}
        for x0, y0 in pip_locations[colour]:
            for x1, y1 in pip_locations[colour]:
                distances[(x0, y0)][(x1, y1)] = max(abs(x0-x1), abs(y0-y1))

        other_pips = (pip for pip in pip_locations[colour] if pip != chosen_pips[colour])
        best_loop_sizes[colour] = float('Inf')
        for order in itertools.permutations(other_pips):
            loop_order = (chosen_pips[colour], *order)
            loop_size = sum(distances[loop_order[i-1]][loop_order[i]] for i in range(len(loop_order)))
            best_loop_sizes[colour] = min(best_loop_sizes[colour], loop_size)

    total_spaces = width*height
    for colour in colours:
        max_conection_lengths[colour] = (total_spaces - sum(best_loop_sizes[other_colour] for other_colour in colours if other_colour != colour))//2

    # print(max_conection_lengths)
    return max_conection_lengths


def build_solution(width, height, assignment, colour_variables, wall_variables, colours, pip_locations):

    coloured_grid = [[None for x in range(width)] for y in range(height)]
    walls = {(x,y):None for y in range(2*height-1) for x in range(2*width-1) if x % 2 and y % 2 and (x+y)%4 == 0}

    for y in range(height):
        for x in range(width):
            for colour in colours:
                if colour_variables[(x,y,colour)] in assignment:
                    coloured_grid[y][x] = colour
    for (x,y) in walls:
        for wall_type in wall_types:
            if wall_variables[(x,y,wall_type)] in assignment:
                walls[(x,y)] = wall_type
    
    solution = {
        'height': height,
        'width': width,
        'coloured_grid': coloured_grid,
        'walls': walls,
        'pip_locations': pip_locations
    }
    return solution
    
def count_walls(walls):
    return len([wall for wall in walls.values() if wall != ' '])

def parse_loops(width, height, walls):
    cells = {(x,y) for x in range(width) for y in range(height)}

    connections = {cell:set() for cell in cells}
    for x in range(width):
        x1 = x^1
        connections[(x,0)].add((x1,0))
        connections[(x,height-1)].add((x1,height-1))
    for y in range(height):
        y1 = y^1
        connections[(0,y)].add((0,y1))
        connections[(width-1,y)].add((width-1,y1))

    for x, y in walls:
        wall_type = walls[(x,y)]
        x0, y0 = x//2, y//2
        x1, y1 = x0+1, y0+1

        if wall_type == ' ':
            connections[(x0, y0)].add((x1,y1))
            connections[(x1, y1)].add((x0,y0))
            connections[(x0, y1)].add((x1,y0))
            connections[(x1, y0)].add((x0,y1))
        elif wall_type == '|':
            connections[(x0, y0)].add((x0,y1))
            connections[(x0, y1)].add((x0,y0))
            connections[(x1, y0)].add((x1,y1))
            connections[(x1, y1)].add((x1,y0))
        elif wall_type == '-':
            connections[(x0, y0)].add((x1,y0))
            connections[(x1, y0)].add((x0,y0))
            connections[(x0, y1)].add((x1,y1))
            connections[(x1, y1)].add((x0,y1))
        else:
            raise Exception('Unrecognised wall type')
        
    loops = []
    while cells:
        starting_element = cells.pop()
        loops.append({starting_element})
        new_cells = {starting_element}

        while new_cells:
            new_cells = {neighbour for new_cell in new_cells for neighbour in connections[new_cell] if neighbour in cells}
            for new_cell in new_cells:
                cells.remove(new_cell)
                loops[-1].add(new_cell)
    return loops

def merge_loops(solution):
    width = solution['width']
    height = solution['height']
    walls = deepcopy(solution['walls'])
    coloured_grid = deepcopy(solution['coloured_grid'])
    pip_locations = deepcopy(solution['pip_locations'])

    original_number_of_loops = len(parse_loops(width, height, walls))

    while True:
        loops = parse_loops(width, height, walls)
        cell_loop = {cell:i for i, loop in enumerate(loops) for cell in loop}

        colour_loops = {colour:set() for colour in pip_locations}
        loop_colours = {i:0 for i in range(len(loops))}
        for colour, pips in pip_locations.items():
            for pip in pips:
                for i, loop in enumerate(loops):
                    if pip in loop:
                        colour_loops[colour].add(i)
                        loop_colours[i] = colour
        # Colours type:
        # 1. Join two loops of same colour
        # 2. Join colourless loop to colour which needs connecting
        # 3. Join colourless loop to any other colour

        # Move:
        # 1. Remove wall
        # 2. Add wall
        options = dict()
        for (x, y), wall_type in walls.items():
            if wall_type == ' ':
                move = 2
            else:
                move = 1
            x_0, y_0 = x//2, y//2
            neighbouring_cells = ((x_0, y_0), (x_0, y_0 + 1), (x_0 + 1, y_0), (x_0 + 1, y_0 + 1))
            wall_loops = {cell_loop[(xi, yi)] for xi, yi in neighbouring_cells}
            if len(wall_loops) == 1:
                continue
            elif len(wall_loops) != 2:
                raise Exception('More than 2 loops neighbouring wall')
            
            wall_loop_colours = {loop_colours[i] for i in wall_loops}

            if len(wall_loop_colours) == 1:
                if 0 in wall_loop_colours:
                    continue
                else:
                    colours_type = 1
            else:
                if 0 in wall_loop_colours:
                    other_colour = [colour for colour in wall_loop_colours if colour != 0][0]
                    if len(colour_loops[other_colour]) == 1:
                        colours_type = 3
                    else:
                        colours_type = 2
                else:
                    continue

            options[(colours_type, move)] = (x, y)

        if options:
            best_option = options[min(options)]
            wall_type = walls[best_option]
            if wall_type == ' ':
                walls[best_option] = random.choice(('-','|'))
            else:
                walls[best_option] = ' '
        else:
            for x in range(width):
                for y in range(height):
                    coloured_grid[y][x] = loop_colours[cell_loop[(x,y)]]
            break


    print(f'Number of loops improved from {original_number_of_loops} to {len(loops)}')

    if len(loops) == len(pip_locations):
        solution = {
            'height': height,
            'width': width,
            'coloured_grid': coloured_grid,
            'walls': walls,
            'pip_locations': pip_locations
        }
        return solution
    else:
        return None
    
def validate_solution(width, height, walls, pip_locations):
    loops = parse_loops(width, height, walls)
    cell_loop = {cell:i for i, loop in enumerate(loops) for cell in loop}
    if len(loops) == len(pip_locations):
        loops_per_colour = [{cell_loop[pip] for pip in pips} for pips in pip_locations.values()]
        if all(len(loop_set) == 1 for loop_set in loops_per_colour):
            loops_used = {list(loop_set)[0] for loop_set in loops_per_colour}
            if len(loops_used) == len(pip_locations):
                return True
    return False

    
def optimise_solution(solution):
    if solution is None:
        return None
    print('Optimising')
    
    width = solution['width']
    height = solution['height']
    walls = deepcopy(solution['walls'])
    coloured_grid = deepcopy(solution['coloured_grid'])
    pip_locations = deepcopy(solution['pip_locations'])

    original_number_of_walls = count_walls(walls)

    while True:

        active_walls = {wall:wall_type for wall,wall_type in walls.items() if wall_type != ' '}
        improvement_flag = False
        for wall0, wall1 in itertools.combinations(active_walls, 2):
            if improvement_flag:
                break
            deactivated_walls = deepcopy(walls)
            deactivated_walls[wall0] = ' '
            deactivated_walls[wall1] = ' '
            if validate_solution(width, height, deactivated_walls, pip_locations):
                walls = deactivated_walls
                improvement_flag = True
            for activated_wall in deactivated_walls:
                if improvement_flag:
                    break
                if deactivated_walls[activated_wall] == ' ':
                    for wall_type in ('-','|'):
                        if improvement_flag:
                            break
                        activated_walls = deepcopy(deactivated_walls)
                        activated_walls[activated_wall] = wall_type
                        if validate_solution(width, height, activated_walls, pip_locations):
                            walls = activated_walls
                            improvement_flag = True
        
        if improvement_flag == False:
            break


    loops = parse_loops(width, height, walls)
    cell_loop = {cell:i for i, loop in enumerate(loops) for cell in loop}
    loop_colours = {i:0 for i in range(len(loops))}
    for colour, pips in pip_locations.items():
        for pip in pips:
            for i, loop in enumerate(loops):
                if pip in loop:
                    loop_colours[i] = colour

    for x in range(width):
        for y in range(height):
            coloured_grid[y][x] = loop_colours[cell_loop[(x,y)]]


    print(f'Number of walls improved from {original_number_of_walls} to {count_walls(walls)}')

    solution = {
        'height': height,
        'width': width,
        'coloured_grid': coloured_grid,
        'walls': walls,
        'pip_locations': pip_locations
    }
    return solution